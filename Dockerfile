# Base image: https://hub.docker.com/_/golang/
FROM debian:stretch
MAINTAINER Vesa-Pekka Palmu <vpalmu@depili.fi>

# Update stretch repositories
RUN sed -i -e 's/deb.debian.org/archive.debian.org/g' \
           -e 's|security.debian.org|archive.debian.org/|g' \
           -e '/stretch-updates/d' /etc/apt/sources.list

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    git \
    subversion \
    bzr \
    cvs \
    mercurial \
    unzip \
    whois \
    ncurses-dev \
    f2fs-tools \
    ca-certificates \
    openssh-client \
    sshpass \
    file \
    sed \
    binutils \
    bash \
    patch \
    gzip \
    bzip2 \
    tar \
    cpio \
    python \
    perl \
    unzip \
    rsync \
    bc \
    wget \
    libssl-dev \
    zip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
